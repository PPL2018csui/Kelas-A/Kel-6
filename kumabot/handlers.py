from . import handler, bot
import re
import logging
from .utils import (
    set_start_range,
    set_range_day_only,
    set_range_month_only,
    set_end_range,
    add_available_schedule,
    add_available_schedule_day,
    add_available_schedule_day_month,
    add_available_schedule_day_time,
    add_available_schedule_month,
    add_available_schedule_month_time,
    add_available_schedule_time,
    start_bot,
    get_solution,
    identify_intent,
    get_summary
    )
from .utils.Scheduler import Group, User
from .message import Message
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
    CarouselTemplate, CarouselColumn, PostbackEvent,
    TemplateSendMessage, PostbackTemplateAction
)
month_dict = {
        'January' : 'Januari',
        'February' : 'Februari',
        'March' : 'Maret',
        'April' : 'April',
        'May' : 'Mei',
        'June' : 'Juni',
        'July' : 'Juli',
        'August' : 'Agustus',
        'September' : 'September',
        'October' : 'Oktober',
        'November' : 'November',
        'December' : 'Desember'
    }

message = Message()
groups = {}
next_handler = {}
users = {}
flag_change_handler = False


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):

    if (event.source.type == 'user'):
        text_r = (
            'Hai! Untuk mendapat rekomendasi jadwal dari Kuma, '
            'undang Kuma ke dalam group atau chat roommu ya.'
        )
        bot.reply_message(
            event.reply_token,
            TextSendMessage(text=text_r))
    else:
        input = event.message.text
        idx = event.source.group_id
        group = None
        try:
            group = groups[idx]
        except:
            group = Group()
            group.id = idx
            groups[idx] = group

        if (bool(re.search('[K|k][U|u][M|m][A|a]{0,1}', input))):
            intent, response = identify_intent(input)
            logging.warning(intent)
            message.process_new_message(intent, response, group, event)


@message.handle_message(intent='create-schedule')
def handler_start_bot(response, group, event):
    result, group = start_bot(group)
    response = ''
    if result == 'success':
        next_handler[group.id] = handler_set_range_start
        response = (
            "Silahkan memasukan tanggal awal dari jangka waktu "
            "yang diinginkan."
        )
    elif result == 'fail':
        response = (
            "Mohon maaf, ada gangguan pada database. "
            "Silahkan masukkan kembali tanggal mulai "
            "yang diinginkan."
        )
    else:
        next_handler[group.id] = handler_set_range_start
        response = (
            "Anda sedang dalam sesi mencari jadwal"
        )

    groups[group.id] = group
    bot.reply_message(
        event.reply_token,
        TextSendMessage(text=response))


@message.handle_message(intent='date-range-start-day')
def handler_set_range_start_day(response, group, event):
    logging.warning('masuk handle start day')
    result, group = set_range_day_only(group, response)
    logging.warning(result)
    groups[group.id] = group
    response = get_response_set_range_start(group, result)

    bot.reply_message(
        event.reply_token,
        TextSendMessage(text=response))


@message.handle_message(intent='date-range-start-month')
def handler_set_range_start_month(response, group, event):
    logging.warning('masuk handle start month')
    result, group = set_range_month_only(group, response)
    groups[group.id] = group
    response = get_response_set_range_start(group, result)

    bot.reply_message(
        event.reply_token,
        TextSendMessage(text=response))


@message.handle_message(intent='date-range-start')
def handler_set_range_start(response, group, event):
    logging.warning('masuk handle start')
    result, group = set_start_range(group, response)
    groups[group.id] = group
    response = get_response_set_range_start(group, result)

    bot.reply_message(
        event.reply_token,
        TextSendMessage(text=response))


# def handler_set_range_start_year(event, group, message):
#     groups[group.id] = group
#     message = "mulai bulan {} tanggal {} tahun {}".format(group.day, group.month, message)
#     result, group = identify_intent(group, message)
#     groups[group.id] = group
#     response = get_response_set_range_start(group, result)
#
#     bot.reply_message(
#         event.reply_token,
#         TextSendMessage(text=response))


@message.handle_message(intent='date-range-end')
def handler_set_range_end(response, group, event):
    logging.warning('masuk handler range end')
    result, group = set_end_range(group, response)
    groups[group.id] = group
    response = get_response_set_range_end(group, result)

    bot.reply_message(
        event.reply_token,
        TextSendMessage(text=response))


@message.handle_message(intent='date-range-end-day')
def handler_set_range_end_day(response, group, event):
    logging.warning('masuk handler range end day')
    result, group = set_range_day_only(group, response)
    logging.warning(result)
    groups[group.id] = group
    response = get_response_set_range_end(group, result)

    bot.reply_message(
        event.reply_token,
        TextSendMessage(text=response))


@message.handle_message(intent='date-range-end-month')
def handler_set_range_end_month(response, group, event):
    logging.warning('masuk handler range end month')
    result, group = set_range_month_only(group, response)
    groups[group.id] = group
    response = get_response_set_range_end(group, result)

    bot.reply_message(
        event.reply_token,
        TextSendMessage(text=response))


# def handler_set_range_end_year(event, group, message):
#     groups[group.id] = group
#     message = "selesai bulan {} tanggal {} {}".format(group.day, group.month, message)
#     result, group = identify_intent(group, message)
#     groups[group.id] = group
#     response = get_response_set_range_end(group, result)

#     bot.reply_message(
#         event.reply_token,
#         TextSendMessage(text=response))


def get_response_set_range_start(group, result):
    if result == 'no-date':
        return (
            "Tolong spesifikasikan tanggalnya dari hari yang diinginkan. "
            "Pada tanggal berapa kamu akan memulai jangka waktu?"
        )
    elif result == 'no-month':
        return "Pada bulan apa kamu akan memulai jangka waktu?"
        "{} {}".format(group.day, group.range_start)
    elif result == 'no-date-month':
        return (
            "Tolong spesifikasikan tanggal dan bulan yang diinginkan. "
            "Pada tanggal berapa dan bulan apa kamu "
            "akan memulai jangka waktu?"
        )
    elif result == 'no-valid':
        return (
            "Mohon maaf, tanggal yang anda masukkan tidak valid. "
            "Silahkan memasukkan kembali tanggal mulai yang diinginkan."
        )
    elif result == 'fail':
        return (
            "Mohon maaf, ada gangguan pada database. "
            "Silahkan masukkan kembali tanggal mulai yang diinginkan."
        )
    elif result == 'success':
        sched = group.range_start
        return (
            "Tanggal awal jangka waktu sudah tersimpan yaitu "
            "{}-{}-{}. "
            "Silahkan masukkan tanggal akhir dari "
            "jangka waktu yang diinginkan."
        ).format(sched.day, sched.month, sched.year)
    else:
        return (
            "Anda sudah memasukkan jangka waktu awal."
            " Silahkan memasukkan jangka waktu akhir."
        )


def get_response_set_range_end(group, result):
    if result == 'no-date':
        return (
            "Tolong spesifikasikan tanggalnya dari hari yang diinginkan. "
            "Pada tanggal berapa kamu akan mengakhiri jangka waktu?"
        )
    elif result == 'no-month':
        return "Pada bulan apa kamu akan mengakhiri jangka waktu?"
    elif result == 'no-date-month':
        return (
            "Tolong spesifikasikan tanggal dan bulan yang diinginkan. "
            "Pada tanggal berapa dan bulan apa kamu "
            "akan mengakhiri jangka waktu?"
        )
    elif result == 'no-valid':
        return (
            "Mohon maaf, tanggal yang anda masukkan tidak valid. "
            "Silahkan memasukkan kembali tanggal akhir yang diinginkan."
        )
    elif result == 'fail':
        return (
            "Mohon maaf, ada gangguan pada database. "
            "Silahkan masukkan kembali tanggal akhir yang diinginkan."
        )
    elif result == 'success':
        sched = group.range_end
        return (
            "Tanggal akhir jangka waktu sudah tersimpan yaitu "
            "{}-{}-{}. "
            "Silahkan masukkan jadwal kosongmu sesuai dengan "
            "jangka waktu yang telah ditetapkan."
        ).format(sched.day, sched.month, sched.year)
    elif result == 'unidentified':
        return (
            "Mohon maaf, Kuma kurang mengerti maksud anda."
        )
    else:
        return (
            "Anda sudah memasukkan jangka waktu akhir."
            " Silahkan memasukkan jadwal kosong anda."
        )


@message.handle_message(intent='available-schedule')
def handler_add_schedule_available(response, group, event):
    user = User()
    user.id = event.source.user_id

    result, user = add_available_schedule(group, user, response)
    group.users[user.id] = user
    groups[group.id] = group

    response = get_response_add_schedule_available(user, result)
    bot.reply_message(
        event.reply_token, response)


@message.handle_message(intent='available-schedule-month')
def handler_add_schedule_available_month(response, group, event):
    user = User()
    user.id = event.source.user_id

    result, user = add_available_schedule_month(group, user, response)
    group.users[user.id] = user
    groups[group.id] = group

    response = get_response_add_schedule_available(user, result)
    bot.reply_message(
        event.reply_token, response)


@message.handle_message(intent='available-schedule-month-time')
def handler_add_schedule_available_month_time(response, group, event):
    user = User()
    user.id = event.source.user_id

    result, user = add_available_schedule_month_time(group, user, response)
    group.users[user.id] = user
    groups[group.id] = group

    response = get_response_add_schedule_available(user, result)
    bot.reply_message(
        event.reply_token, response)


@message.handle_message(intent='available-schedule-day-month')
def handler_add_schedule_availableday_day_month(response, group, event):
    user = User()
    user.id = event.source.user_id

    result, user = add_available_schedule_day_month(group, user, response)
    group.users[user.id] = user
    groups[group.id] = group

    response = get_response_add_schedule_available(user, result)
    bot.reply_message(
        event.reply_token, response)


@message.handle_message(intent='available-schedule-day-time')
def handler_add_schedule_available_day_time(response, group, event):
    user = User()
    user.id = event.source.user_id

    result, user = add_available_schedule_day_time(group, user, response)
    group.users[user.id] = user
    groups[group.id] = group

    response = get_response_add_schedule_available(user, result)
    bot.reply_message(
        event.reply_token, response)


@message.handle_message(intent='available-schedule-day')
def handler_add_schedule_available_day(response, group, event):
    user = User()
    user.id = event.source.user_id

    logging.warning(group.range_start)
    result, user = add_available_schedule_day(group, user, response)
    group.users[user.id] = user
    groups[group.id] = group

    response = get_response_add_schedule_available(user, result)
    bot.reply_message(
        event.reply_token, response)


@message.handle_message(intent='available-schedule-time')
def handler_add_schedule_available_time(response, group, event):
    user = User()
    user.id = event.source.user_id
    result, user = add_available_schedule_time(user, response)

    group.users[user.id] = user
    groups[group.id] = group

    response = get_response_add_schedule_available(user, result)
    bot.reply_message(
        event.reply_token, response)


def parse_date(text):
    if(bool(re.search('\s', text))):
        _, date = text.replace('\s', '-').split('-')
        return tuple(map(int, date))
    else:
        return tuple(map(int, text.split('-')))


def next_handler_available_day(response, group, event):
    user = group.users[event.source.user_id]
    result, user = add_available_schedule_day(group, user, response)

    group.users[user.id] = user
    groups[group.id] = group

    response = get_response_add_schedule_available(user, result)
    bot.reply_message(
        event.reply_token, response)


def next_handler_available_time(response, group, event):
    user = group.users[event.source.user_id]
    result, user = add_available_schedule_time(group, user, response)

    group.users[user.id] = user
    groups[group.id] = group

    response = get_response_add_schedule_available(user, result)
    bot.reply_message(
        event.reply_token, response)


def next_handler_available_month(response, group, event):
    user = group.users[event.source.user_id]
    result, user = add_available_schedule_month(group, user, response)

    group.users[user.id] = user
    groups[group.id] = group

    response = get_response_add_schedule_available(user, result)
    bot.reply_message(
        event.reply_token, response)


def get_response_add_schedule_available(user, result):
    if result == 'ask-time':
        message.register_next_handler(user.id, next_handler_available_time)
        return TextSendMessage(text=(
            "Untuk tanggal {}, bulan {}, pada jam berapa kamu "
            "memiliki waktu kosong?"
        ).format(user.day, user.month))
    elif result == 'no-valid':
        return TextSendMessage(text=(
            "Mohon maaf, tanggal yang anda masukkan tidak valid. "
            "Silahkan memasukkan kembali tanggal jadwal kosong anda."))
    elif result == 'fail':
        return TextSendMessage(text=(
            "Mohon maaf, ada gangguan pada database. "
            "Silahkan memasukkan kembali tanggal jadwal kosong anda."))
    elif result == 'success':
        sched = user.schedule_avb
        return TextSendMessage(text=(
            "Tanggal jadwal kosongmu sudah tersimpan yaitu "
            "{}-{}-{} pada jam {}:{}."
        ).format(sched.day, sched.month, sched.year, sched.hour, sched.minute))
    elif result == 'already-set':
        return TextSendMessage(text=(
            "Anda sudah memasukkan jadwal yang sama sebelumnya."))
    elif result == 'ask-date':
        message.register_next_handler(user.id, next_handler_available_day)
        if len(user.schedule_avb) > 5:
            user.schedule_avb = user.schedule_avb[0:5]
        dates = ', '.join([str(x.day) for x in user.schedule_avb])
        return TextSendMessage(text=(
            "Dari hari yang sudah anda pilih terdapat beberapa tanggal dengan"
            " hari yang sama yaitu tanggal {}.\n"
            "Pada tanggal berapa kamu memiliki jadwal kosong?").format(dates))
    elif result == 'ask-date-time':
        message.register_next_handler(user.id, next_handler_available_day)
        return TextSendMessage(text=(
            "Pada tanggal berapa kamu memiliki waktu kosong di"
            " jam {}:{}?").format(user.time[0], user.time[1]))
    elif result == 'ask-month':
        message.register_next_handler(user.id, next_handler_available_month)
        if len(user.schedule_avb) > 5:
            user.schedule_avb = user.schedule_avb[0:5]
        months = ', '.join([month_dict[x.strftime("%B")] for x in user.schedule_avb])
        return TextSendMessage(text=(
            "Pada tanggal yang kamu pilih, terdapat tanggal di beberapa"
            "bulan yang berbeda yaitu pada Bulan {}.\n"
            "Pada Bulan apa kamu memiliki waktu kosong?").format(months))
    elif result == 'ask-dates':
        message.register_next_handler(user.id, next_handler_available_day)
        return TextSendMessage(text=(
            "Pada tanggal berapa kamu memiliki waktu kosong di"
            " Bulan {} jam {}:{}?").format(user.month, user.time[0], user.time[1]))
    elif result == 'ask-dates-month':
        message.register_next_handler(user.id, next_handler_available_day)
        return TextSendMessage(text=(
            "Pada tanggal berapa kamu memiliki waktu kosong di"
            " Bulan {}?").format(user.month))
    else:
        message.register_next_handler(user.id, next_handler_available_month)
        return TextSendMessage(text=(
            "Pada bulan apa kamu memiliki waktu kosong di"
            " tanggal {} jam {}:{}?").format(user.day, user.time[0], user.time[1]))


@message.handle_message(intent='end-schedule')
def handler_give_solution(response, group, event):
    res = get_summary(group)
    if res == 'no-schedule':
        bot.reply_message(
            event.reply_token,
            TextSendMessage(text=(
                "Mohon maaf, belum ada jadwal kosong yang dimasukkan."
            )))
    else:
        response, group = get_solution(group)

        if response == 'no-solution':
            bot.reply_message(
                event.reply_token,
                TextSendMessage(text=(
                    "Mohon maaf tidak ada solusi untuk jadwal bertemu kalian."
                )))
        else:
            bot.reply_message(
                event.reply_token,
                TextSendMessage(text=(
                    "Berikut rangkuman jadwal anda:\n {}"
                    "Solusi jadwal bertemu kalian adalah {}"
                ).format(res, response)))


message.handle_message(intent='Default Fallback Intent')
def handler_give_solution(response, group, event):
    bot.reply_message(
        event.reply_token,
        TextSendMessage(text=(
            "Maaf, Kuma kurang mengerti maksud anda. Silahkan input kembali."
        )))
