from . import db
from sqlalchemy.dialects.postgresql import DATE, TIMESTAMP, VARCHAR, ARRAY


class Schedulers(db.Model):
    __tablename__ = 'scheduler'

    id_group = db.Column(VARCHAR(50), primary_key=True)
    range_start = db.Column(DATE())
    range_end = db.Column(DATE())

    def __init__(self, id_group, range_start, range_end):
        self.id_group = id_group
        self.range_start = range_start
        self.range_end = range_end

    def __repr__(self):
        return '<id group {}>'.format(self.id_group)


class GroupSchedule(db.Model):
    __tablename__ = 'group_schedule'

    id_group = db.Column(VARCHAR(50), db.ForeignKey('scheduler.id_group'), primary_key=True)
    schedule = db.Column(TIMESTAMP(True), primary_key=True)
    id_user = db.Column(VARCHAR(50), primary_key=True)

    def __init__(self, id_group, schedule, id_user):
        self.id_user = id_user
        self.schedule = schedule
        self.id_group = id_group

    def __repr__(self):
        return '<id user {}, schedule {}>'.format(self.id_user, self.schedule)
