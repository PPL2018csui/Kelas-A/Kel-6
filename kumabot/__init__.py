from flask import Flask, request, abort
import sys
from argparse import ArgumentParser
from flask_sqlalchemy import SQLAlchemy
import apiai

from linebot import (
    LineBotApi, WebhookHandler
)

from linebot.exceptions import (
    InvalidSignatureError
)

app = Flask(__name__)
app.config.from_object('{}.config'.format(__name__))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

# get channel_secret and channel_access_token from your environment variable
channel_secret = app.config['LINE_CHANNEL_SECRET']
channel_access_token = app.config['LINE_CHANNEL_ACCESS_TOKEN']
if channel_secret is None:
    print('Specify LINE_CHANNEL_SECRET as environment variable.')
    sys.exit(1)
if channel_access_token is None:
    print('Specify LINE_CHANNEL_ACCESS_TOKEN as environment variable.')
    sys.exit(1)
client_access_token = app.config['CLIENT_ACCESS_TOKEN']

bot = LineBotApi(channel_access_token)
handler = WebhookHandler(channel_secret)
ai = apiai.ApiAI(client_access_token)

# Configure application logging
from . import handlers  # noqa
from .models import Schedulers, GroupSchedule

app.logger.setLevel(app.config['LOG_LEVEL'])


@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
        return 'OK'
    except InvalidSignatureError as e:
        print(e)
        return abort(400)


if __name__ == "__main__":
    arg_parser = ArgumentParser(
        usage='Usage: python ' + __file__ + ' [--port <port>] [--help]'
    )
    arg_parser.add_argument('-p', '--port', type=int, default=8000, help='port')
    arg_parser.add_argument('-d', '--debug', default=False, help='debug')
    options = arg_parser.parse_args()
    app.run(debug=options.debug, port=options.port)
