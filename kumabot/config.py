from os import environ
from dotenv import load_dotenv
load_dotenv('./.env')

APP_ENV = environ.get('APP_ENV', 'development')
DEBUG = environ.get('DEBUG', 'true') == 'true'
LINE_CHANNEL_SECRET = environ.get('LINE_CHANNEL_SECRET', 'something')
LINE_CHANNEL_ACCESS_TOKEN = environ.get('LINE_CHANNEL_ACCESS_TOKEN', 'something')
LOG_LEVEL = environ.get('LOG_LEVEL', 'DEBUG')
WEBHOOK_HOST = environ.get('WEBHOOK_HOST', '127.0.0.1')
DATABASE_URL = environ.get('DATABASE_URL')
SQLALCHEMY_DATABASE_URI = environ.get('DATABASE_URL')
CLIENT_ACCESS_TOKEN = environ.get('CLIENT_ACCESS_TOKEN')
