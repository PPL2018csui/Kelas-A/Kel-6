class Message:

    def __init__(self):
        self.message_handlers = []
        self.pre_message_subscribers_next_step = {}

    def process_new_message(self, intent, response, group, event):
        next = self._notify_pre_next_step_handler(response, group, event)
        if next is False:
            self._notify_new_message_to_handler(self.message_handlers,
                                            intent, response, group, event)

    def _notify_new_message_to_handler(self, message_handlers, intent, response, group, event):
        for handler in message_handlers:
            if self._test_function(handler, intent):
                self._exec_func(handler['function'], response, group, event)

    def _exec_func(self, task, response, group, event):
        task(response, group, event)

    def _test_function(self, handler, intent):
        return handler['filter'] == intent

    def _build_handler_dict(self, handler, filter):
        return {
            'function': handler,
            'filter': filter
        }

    def handle_message(self, intent):
        def decorator(handler):
            handler_dict = self._build_handler_dict(handler, intent)
            self.add_message_handler(handler_dict)

            return handler
        return decorator

    def add_message_handler(self, handler_dict):
        self.message_handlers.append(handler_dict)

    def register_next_handler(self, user_id, callback):
        self.pre_message_subscribers_next_step[user_id] = callback

    def _notify_pre_next_step_handler(self, response, group, event):
        k = event.source.user_id
        status = True
        if k in self.pre_message_subscribers_next_step.keys():
            self._exec_next_handler_func(self.pre_message_subscribers_next_step[k], response, group, event)
        else:
            status = False
        self.pre_message_subscribers_next_step = {}

        return status

    def _exec_next_handler_func(self, task, response, group, event):
        task(response, group, event)
