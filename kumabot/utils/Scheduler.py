from kumabot.utils.Summary import Summary
from kumabot import db
from kumabot.models import Schedulers, GroupSchedule
from kumabot import ai
import json
import logging


class Group:

    def __init__(self):
        self.id = None
        self.users = {}
        self.day = None
        self.month = None
        self.year = None
        self.range_start = None
        self.range_end = None


class User:

    def __init__(self):
        self.id = None
        self.day = None
        self.month = None
        self.time = None
        self.schedule_avb = None


class Scheduler:

    def __init__(self):
        self.scheduler = None
        self.schedules_avb = None
        self.summary = None
        self.table_scheduler = 'scheduler'
        self.table_schedule_coll = 'schedule_collection'

    def get_schedule_summaries_from_db(self, group):
        id = group.id
        try:
            query = db.session.query(GroupSchedule).filter_by(id_group=id)
            self.schedules_avb = query
            db.session.commit()
            return True
        except:
            return False

    def get_schedulers_from_db(self, group):
        id = group.id
        try:
            query = db.session.query(Schedulers).filter_by(id_group=id)
            self.scheduler = query
            db.session.commit()
            logging.warning('bisa get_schedulers_from_db')
            return True
        except:
            logging.warning('gagal get_schedulers_from_db')
            return False

    def set_range_start(self, group):
        id = group.id
        start = group.range_start
        self.get_schedulers_from_db(group)
        if self.scheduler[0].range_start is not None:
            return 'already-set'
        try:
            record = db.session.query(Schedulers).filter_by(id_group=id).one()
            # logging.warning(record)
            record.range_start = start
            db.session.commit()
            return 'success'
        except:
            return 'fail'

    def set_range_end(self, group):
        id = group.id
        end = group.range_end
        self.get_schedulers_from_db(group)
        if self.scheduler[0].range_start is None:
            return 'start-not-set'
        if self.scheduler[0].range_end is not None:
            return 'already-set'
        try:
            record = db.session.query(Schedulers).filter_by(id_group=id).one()
            record.range_end = end
            db.session.commit()
            return 'success'
        except:
            return 'fail'

    def start_new_scheduler(self, group):
        hasil = self.get_schedulers_from_db(group)
        print(hasil)
        id = group.id
        schedulers = Schedulers(
            id_group=id,
            range_start=None,
            range_end=None
        )
        print('siap insert')
        return self.__insert(schedulers)

    def make_summary(self, group):
        id = group.id
        self.summary = Summary()

        for group_schedule in self.schedules_avb:
            self.summary.add_schedule(group_schedule[1])
        try:
            res = self.summary.list_schedule()
            return res
        except:
            return []

    def add_available_schedule(self, group, user):
        self.get_schedule_summaries_from_db(group)
        for scd in self.schedules_avb:
            if scd.schedule == user.schedule_avb:
                return 'already-set'
        id = group.id
        schedule_avb = user.schedule_avb
        user = user.id
        group_schedule = GroupSchedule(
            id_group=id,
            schedule=schedule_avb,
            id_user=user
        )

        return self.__insert(group_schedule)

    def get_message_json(self, message):
        request = ai.text_request()
        request.query = message
        request.lang = 'id'
        byte_response = request.getresponse().read()
        json_response = byte_response.decode('utf8').replace("'", '"')
        response = json.loads(json_response)

        return response

    def __insert(self, table):
        try:
            db.session.add(table)
            print('bisa add insert')
            print(table)
            db.session.commit()
            print('bisa commit insert')
            return 'success'
        except:
            return 'fail insert'

    def delete_scheduler(self, group):
        id = group.id
        try:
            record1 = db.session.query(GroupSchedule).filter_by(id_group=id).one()
            db.session.delete(record1)
            record2 = db.session.query(Schedulers).filter_by(id_group=id).one()
            db.session.delete(record2)
            db.session.commit()
            return True
        except:
            return False
