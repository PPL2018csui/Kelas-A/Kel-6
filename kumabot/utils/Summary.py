class Summary:

    def __init__(self, schedule=dict({})):
        self.__schedule = schedule

    def list_schedule(self):
        summary_list = []
        for k, v in self.__schedule.items():
            summary_list.append('{}: {}'.format(k, v))
        return summary_list

    def add_schedule(self, schedule_date):
        if schedule_date in self.__schedule:
            val = self.__schedule[schedule_date]
            self.__schedule.update({"%s" % schedule_date: val+1})
        else:
            self.__schedule.update({"%s" % schedule_date: 1})

    def get_schedules(self):
        return self.__schedule
