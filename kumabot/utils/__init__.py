from kumabot.utils.Scheduler import Scheduler
from datetime import timedelta
from dateutil.relativedelta import *
from time import strptime
from kumabot.utils.Summary import Summary
import datetime
import logging
import calendar
import time
datetime_dict = {
    'bulan depan': 1,
    'dua bulan lagi': 2,
    'bulan ini': 0,
    'besok': 1,
    'lusa': 2
}
today = datetime.date.today()


def add_available_schedule(group, user, response):
    scheduler = Scheduler()
    day = response['result']['parameters']['day']
    month = response['result']['parameters']['month']
    hour, minute, _ = parse_time(response['result']['parameters']['time'])
    range_start = group.range_start
    range_end = group.range_end
    year = range_end.year

    date = None
    try:
        day = int(day)
        user.schedule_avb = datetime.datetime(year, int(month), int(day),
                                 hour=hour, minute=minute)
    except:
        day_avb = []
        date = datetime.datetime(range_start.year, int(month), range_start.day,
                                 hour=hour, minute=minute)
        while date.date() <= range_end:
            if date.strftime("%A") == day:
                day_avb.append(date)
                date += timedelta(days=7)
            else:
                date += timedelta(days=1)
        if len(day_avb) == 0:
            return('no-valid', user)
        elif len(day_avb) > 1:
            user.schedule_avb = day_avb
            return ('ask-date', user)
        else:
            user.schedule_avb = day_avb[0]

    if check_valid_available_schedule(group, user.schedule_avb.date()) is False:
        return ('no-valid', user)

    return (scheduler.add_available_schedule(group, user), user)


def start_bot(group):
    scheduler = Scheduler()
    return (scheduler.start_new_scheduler(group), group)


def is_date_valid(group, response, is_start_range):
    day = response['result']['parameters']['day']
    month = response['result']['parameters']['month']

    try:
        day = int(day)
        group.day = day
        try:
            month = int(month)
        except:
            month = today.month + datetime_dict[month]
        group.month = month
    except ValueError:
        return ('no-date', group)

    input = datetime.date(today.year, month, day)
    if month < today.month:
        try:
            year_exist = int(response['result']['parameters']['year'])
            input.replace(year=year_exist)
        except:
            return ('ask-year', group)

    group.year = input.year
    if input < today:
        return ('no-valid', group)
    else:
        if is_start_range:
            group.range_start = input
        else:
            group.range_end = input
        return ('valid', group)


def set_start_range(group, response):
    status, group = is_date_valid(group, response, True)
    print(status)
    if (status != 'valid'):
        return (status, group)
    SCD = Scheduler()
    group.range_start = datetime.date(group.year, group.month, group.day)
    logging.warning(group.range_start)
    response = SCD.set_range_start(group)
    logging.warning(response)
    return (response, group)


def set_range_day_only(group, response):
    day = response['result']['parameters']['day']

    try:
        day = int(day)
        group.day = day
        return ('no-month', group)
    except:
        return ('no-date-month', group)


def set_range_month_only(group, response):
    month = response['result']['parameters']['month']

    try:
        month = int(month)
    except:
        month = today.month + datetime_dict[month]

    group.month = month
    return ('no-date', group)


def set_end_range(group, response):
    status, group = is_date_valid(group, response, False)
    if (status != 'valid'):
        return (status, group)
    SCD = Scheduler()
    group.range_end = datetime.date(group.year, group.month, group.day)
    response = SCD.set_range_end(group)
    logging.warning(response)
    return (response, group)


def check_valid_available_schedule(group, date_available):
    return group.range_start <= date_available <= group.range_end


def add_available_schedule_day(group, user, response):
    day = response['result']['parameters']['day']
    range_start = group.range_start
    range_end = group.range_end
    scheduler = Scheduler()

    date = None
    try:
        day = int(day)
        avb = [x for x in user.schedule_avb if x.day == day]
        try:
            avb[0].hour
            user.schedule_avb = avb[0]
        except:
            user.day = avb[0].day
            user.month = avb[0].month
            return ('ask-time', user)
    except TypeError:
        user.day = day
        month = user.month if user.month is not None else range_start.month
        date = datetime.date(range_start.year, month, day)
        if user.time is not None:
            date = datetime.datetime(range_start.year, month, day,
                user.time[0], user.time[0])
        day_avb = []
        if type(date) is datetime.date:
            tmp = date
        else:
            tmp = date.date()
        while tmp <= range_end:
            day_avb.append(date)
            date += relativedelta(months=+1)
            if type(date) is datetime.date:
                tmp = date
            else:
                tmp = date.date()
        if len(day_avb) == 0:
            print('what')
            return('no-valid', user)
        elif len(day_avb) > 1:
            if user.month is not None and user.time is not None:
                user.schedule_avb = day_avb[0]
            elif user.month is not None:
                user.schedule_avb = day_avb
                return ('ask-time', user)
            else:
                user.schedule_avb = day_avb
                return ('ask-month', user)
        else:
            user.schedule_avb = day_avb[0]
            if type(date) is datetime.date:
                user.schedule_avb = day_avb
                user.day = user.schedule_avb[0].day
                user.month = user.schedule_avb[0].month
                return('ask-time', user)
    except ValueError:
        day_avb = []
        date = datetime.date(range_start.year, range_start.month, range_start.day)
        while date <= range_end:
            if date.strftime("%A") == day:
                day_avb.append(date)
                date += timedelta(days=7)
            else:
                date += timedelta(days=1)
        user.schedule_avb = day_avb
        if len(day_avb) == 0:
            print('whet')
            return('no-valid', user)
        elif len(day_avb) > 1:
            return ('ask-date', user)
        else:
            return('ask-time', user)

    if check_valid_available_schedule(group, user.schedule_avb.date()) is False:
        return ('no-valid', user)

    return (scheduler.add_available_schedule(group, user), user)


def add_available_schedule_day_month(group, user, response):
    day = response['result']['parameters']['day']
    month = response['result']['parameters']['month']
    range_start = group.range_start
    range_end = group.range_end

    user.month = int(month)
    try:
        user.day = int(day)
        user.schedule_avb = [datetime.date(range_start.year, user.month, user.day)]
    except:
        return('ask-dates-month', user)

    return ('ask-time', user)


def add_available_schedule_day_time(group, user, response):
    day = response['result']['parameters']['day']
    hour, minute, sec = parse_time(response['result']['parameters']['time'])
    range_start = group.range_start
    range_end = group.range_end

    user.time = (hour, minute, sec)
    try:
        user.day = int(day)
    except:
        return('ask-date-time', user)

    return ('ask-months', user)


def add_available_schedule_month(group, user, response):
    month = response['result']['parameters']['month']
    range_start = group.range_start
    range_end = group.range_end
    scheduler = Scheduler()

    month = int(month)
    user.month = month
    day = user.day if user.day is not None else range_start.day
    date = datetime.date(range_start.year, month, day)
    day_avb = []
    while date <= range_end:
        day_avb.append(date)
        date += timedelta(days=1)
    if len(day_avb) == 0:
        return('no-valid', user)
    elif len(day_avb) > 1:
        if user.day is not None and user.time is not None:
            user.schedule_avb = datetime.datetime(day_avb[0].year, day_avb[0].month,
                    day_avb[0].day, user.time[0], user.time[1])
            return (scheduler.add_available_schedule(group, user), user)
        if user.day is not None:
            user.schedule_avb = day_avb
            user.day = day_avb[0].day
            user.month = day_avb[0].month
            return ('ask-time', user)
        else:
            return ('ask-dates-month', user)
    else:
        if user.time is not None:
            user.schedule_avb = datetime.datetime(day_avb[0].year, day_avb[0].month,
                    day_avb[0].day, user.time[0], user.time[1])
            return (scheduler.add_available_schedule(group, user), user)
        user.schedule_avb = day_avb
        user.day = day_avb[0].day
        user.month = day_avb[0].month

    return ('ask-time', user)


def add_available_schedule_month_time(group, user, response):
    month = response['result']['parameters']['month']
    hour, minute, sec = parse_time(response['result']['parameters']['time'])
    range_start = group.range_start
    range_end = group.range_end
    scheduler = Scheduler()

    user.month = int(month)
    user.time = (hour, minute, sec)
    return ('ask-dates', user)


def add_available_schedule_time(group, user, response):
    hour, minute, sec = parse_time(response['result']['parameters']['time'])
    scheduler = Scheduler()
    range_start = group.range_start
    range_end = group.range_end

    if user.day is not None and user.month is not None:
        date_avb = [x for x in user.schedule_avb if x.day == user.day and x.month == user.month]
        user.schedule_avb = datetime.datetime(date_avb[0].year, date_avb[0].month,
                                date_avb[0].day, hour=hour, minute=minute)
        return (scheduler.add_available_schedule(group, user), user)
    user.time = (hour, minute, sec)
    return('ask-date-time', user)


# method digunakan untuk parsing pada return value time
def parse_time(time):
    logging.warning('time')
    _, time, _ = time.replace('T', '+').split('+')
    hour, min, sec = time.split(':')
    return (int(hour), int(min), int(sec))


def get_summary(group):
    scheduler = Scheduler()
    scheduler.get_schedule_summaries_from_db(group)
    summary = scheduler.make_summary(group)
    if(len(summary) == 0):
        return 'no-schedule'

    result = ''
    itr = 1
    for x in summary:
        result += '{}. {}\n'.format(itr, x)
        itr += 1

    return result


def get_solution(group):
    scheduler = Scheduler()
    scheduler.get_schedule_summaries_from_db(group)
    scheduler.summary = Summary()
    for group_schedule in scheduler.schedules_avb:
        scheduler.summary.add_schedule(group_schedule[1])

    summary = scheduler.summary.get_schedules()
    if(len(summary) == 0):
        return ('no-schedule', group)

    user_amount = 0
    res = ''
    for k, v in summary.items():
        if v > user_amount:
            user_amount = v
            res = k

    scheduler.delete_scheduler(group)
    users = list(summary.keys())
    logging.warning(user_amount)
    logging.warning(len(users))
    if(user_amount < len(users)):
        return ('no-solution', group)

    scheduler.delete_scheduler(group)
    return (res, group)


def identify_intent(message):
    scheduler = Scheduler()
    response = scheduler.get_message_json(message)
    return (response['result']['metadata']['intentName'], response)
