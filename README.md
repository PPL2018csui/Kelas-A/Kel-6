# Project Title

Kuma (Kumpul Bersama), adalah sebuah bot LINE yang diprogram untuk membantu mencari solusi atas masalah pencarian waktu pertemuan untuk penggunanya.

## Getting Started

Bagian ini menjelaskan tahap-tahap setup untuk masa development Kuma.

### Prerequisites

Kuma dibangun dengan bahasa pemrograman Python, sehingga dibutuhkan development tools Python untuk memprogram Kuma. Penggunaan database di masa development akan menggunakan Postgre. Modul-modul yang dibutuhkan, termasuk framework Flask, sudah tercatat di file requirements.txt. Untuk install bisa menggunakan pip:

```
pip install -r requirements.txt
```

### Installing

modul-modul dari requirements akan berjalan dalam sebuah Python environment.

```
virtualenv venv
```

Kemudian aktivasi environment saat ingin menjalankan Kuma pada local.

```
source env/Scripts/activate
```

Schema dan tabel relasi database local sudah dibuatkan dalam file bot/model.py. Import ke database local dengan init:

```
python manage_check.py init
```
kemudian migrate:
```
python manage_check.py migrate
```
Akan muncul folder migrate untuk database local development. Jika ada perubahan pada schema, programmer lain dapat menyesuaikan perubahan dengan upgrade:
```
python manage_check.py upgrade
```

## Running the tests

Bagian ini menjelaskan cara melakukan automated tests.

### Linter and Code Coverage

Linters mengecek apakah penulisan program sudah sesuai best practice:

```
python manage_check.py check
```
Code coverage mengecek apakah setiap baris code dalam folder bot berjalan ketika seluruh unit test dijalankan.
```
python manage_check.py test
```

## Deployment

Repositori ini sudah terintegrasi dengan aplikasi heroku Kuma. Branch master terhubung dengan heroku deploy Kuma, branch sit_uat terhubung dengan heroku testing Kuma, dan branch userStory terhubung dengan heroku development. Untuk melakukan deploy dapat merge fitur terkait dengan sit_uat, kemudian branch master.

## Built With

* [Flask](http://flask.pocoo.org/docs/0.12/) - Framework yang digunakan

## Contributing

Proyek ini belum membuka akses untuk kontribusi.

## Versioning

Kami menggunakan GitFlow pada [GitLab](https://about.gitlab.com/) untuk version control.

## Authors

**Kelompok PPL A6**
* Faridah Nur Suci A.
* Fellita Candini
* Novianti Aliasih
* Rizky Putri Meiliasari
* Syifa Nurhayati

## License

Belum ada lisensi untuk proyek ini.