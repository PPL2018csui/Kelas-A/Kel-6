import apiai
import datetime, json
from datetime import timedelta

date = datetime.date(2018, 6, 25)
date += timedelta(days=7)
print(date)

smt = None


ai = apiai.ApiAI('56868e410f4647c9b2f24530a4d94170')
request = ai.text_request()
request.query = 'aku bisa tanggal 25 juni'

byte_response = request.getresponse().read()
json_response = byte_response.decode('utf8').replace("'", '"')
response = json.loads(json_response)

print(response['result']['metadata']['intentName'])
