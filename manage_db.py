#!/usr/bin/env python

from os.path import join, dirname
from flask_script import Manager
from flask_migrate import MigrateCommand, Migrate
from dotenv import load_dotenv


def load_app():
    # Environment variable MUST be set before importing the app
    dotenv_path = join(dirname(__file__), '.env')
    load_dotenv(dotenv_path)

    from kumabot import app
    return app


def load_db():
    # Environment variable MUST be set before importing the app
    dotenv_path = join(dirname(__file__), '.env')
    load_dotenv(dotenv_path)

    from kumabot import db
    return db


app = load_app()
db = load_db()
app.config.from_object('{}.config'.format('kumabot'))
migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    manager.run()
