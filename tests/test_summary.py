from kumabot.utils.Summary import Summary

summary = Summary({'2-2-2018': 2, '4-2-2018': 1})
summary_sort = ['2-2-2018: 2', '4-2-2018: 1']
new_sched = '4-2-2018'
add_sum = {'2-2-2018': 2, '4-2-2018': 1}


class TestListSchedule:

    def test_list_schedule(self):
        res = False
        for i in summary.list_schedule():
            for j in summary_sort:
                if (i == j):
                    res = True

        assert res is True

    def test_add_schedule(self):
        res = False
        # tmp_prev_sum = summary.get_schedules()
        summary.add_schedule(new_sched)
        for k, v in summary.get_schedules().items():
            if (k == new_sched and (v == 1 or v == add_sum[new_sched] + 1)):
                res = True
                break
        assert res is True
