from unittest.mock import Mock
import datetime
from linebot.models import (
    TextSendMessage,
    # CarouselTemplate, CarouselColumn, TemplateSendMessage,
    # PostbackTemplateAction
)
from kumabot.handlers import (
    handle_message,
    # handler_set_range_start,
    # handler_set_range_start_year,
    # handler_set_range_start_month,
    # handler_set_range_start_day,
    handler_start_bot,
    # handler_set_range_end,
    # handler_set_range_end_year,
    # handler_set_range_end_month,
    # handler_set_range_end_day,
    handler_add_schedule_available,
    handler_add_schedule_available_time,
    # handler_add_schedule_available_date,
    # handler_give_solution,
    # handler_add_available_schedule_day,
    next_handler_available_day,
    next_handler_available_month,
    next_handler_available_time,
    message
)
from kumabot.utils.Scheduler import Group, User
# from kumabot import handlers
group = Group()
group.id = '123'
group.day = 25
group.month = 6
group.year = 2018
groups_mock = {}
users_mock = {}
groups_mock['123'] = group
user = User()
user.id = 'abc'
user.day = 26
user.month = 6
user.schedule_avb = datetime.datetime(2018, 6, 26, hour=7, minute=30)
group.range_start = datetime.date(2018, 6, 25)
group.range_end = datetime.date(2018, 6, 27)
user.time = (7, 30, 0)
date = ['25-6-2018', '26-6-2018', '27-6-2018']


def test_handle_personal_chat(mocker):
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mock_message = Mock(text='asdjhkjsha')
    mock_message.source.type = 'user'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    text = (
            'Hai! Untuk mendapat rekomendasi jadwal dari Kuma, '
            'undang Kuma ke dalam group atau chat roommu ya.'
        )
    expected_text = (
        TextSendMessage(text)
    )
    assert args[1].text == expected_text.text


def test_handle_start_bot(mocker):
    response = (
        "Silahkan memasukan tanggal awal dari jangka waktu "
        "yang diinginkan."
    )
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('create-schedule', 'defaut response'))
    mocker.patch('kumabot.handlers.start_bot', return_value=('success', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'buat jadwal kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_start_bot_fail(mocker):
    response = (
        "Mohon maaf, ada gangguan pada database. "
        "Silahkan masukkan kembali tanggal mulai "
        "yang diinginkan."
    )
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('create-schedule', 'defaut response'))
    mocker.patch('kumabot.handlers.start_bot', return_value=('fail', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'buat jadwal kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_start_bot_already_set(mocker):
    response = (
        "Anda sedang dalam sesi mencari jadwal"
    )
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('create-schedule', 'defaut response'))
    mocker.patch('kumabot.handlers.start_bot', return_value=('already-set', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'buat jadwal kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_set_range_start_success(mocker):
    response = (
        "Tanggal awal jangka waktu sudah tersimpan yaitu "
        "25-6-2018. "
        "Silahkan masukkan tanggal akhir dari "
        "jangka waktu yang diinginkan."
    )
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('date-range-start', 'default response'))
    mocker.patch('kumabot.handlers.set_start_range', return_value=('success', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'kuma mulai 25 juni 2018'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_set_range_start_failed_db_error(mocker):
    response = (
        "Mohon maaf, ada gangguan pada database. "
        "Silahkan masukkan kembali tanggal mulai yang diinginkan."
    )
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('date-range-start', 'default response'))
    mocker.patch('kumabot.handlers.set_start_range', return_value=('fail', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'kuma mulai 25 juni 2018'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_set_range_start_no_date(mocker):
    response = (
        "Tolong spesifikasikan tanggalnya dari hari yang diinginkan. "
        "Pada tanggal berapa kamu akan memulai jangka waktu?"
    )
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('date-range-start-month', 'default response'))
    mocker.patch('kumabot.handlers.set_range_month_only', return_value=('no-date', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'kuma mulai juni'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_set_range_start_no_month(mocker):
    response = "Pada bulan apa kamu akan memulai jangka waktu?"
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('date-range-start-day', 'default response'))
    mocker.patch('kumabot.handlers.set_range_day_only', return_value=('no-month', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'kuma mulai tanggal 25'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_set_range_start_no_date_month(mocker):
    response = (
        "Tolong spesifikasikan tanggal dan bulan yang diinginkan. "
        "Pada tanggal berapa dan bulan apa kamu "
        "akan memulai jangka waktu?"
    )
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('date-range-start-day', 'default response'))
    mocker.patch('kumabot.handlers.set_range_day_only', return_value=('no-date-month', group))

    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'kuma mulai senin'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_set_range_start_no_valid(mocker):
    response = (
        "Mohon maaf, tanggal yang anda masukkan tidak valid. "
        "Silahkan memasukkan kembali tanggal mulai yang diinginkan."
    )
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('date-range-start', 'default response'))
    mocker.patch('kumabot.handlers.set_start_range', return_value=('no-valid', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'kuma mulai 25 april 2018'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_set_range_end_success(mocker):
    response = (
        "Tanggal akhir jangka waktu sudah tersimpan yaitu "
        "27-6-2018. "
        "Silahkan masukkan jadwal kosongmu sesuai dengan "
        "jangka waktu yang telah ditetapkan."
    ).format(group.range_end)
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('date-range-end', 'default response'))
    mocker.patch('kumabot.handlers.set_end_range', return_value=('success', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'kuma sampai 27 juni 2018'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_set_range_end_no_date(mocker):
    group.month = 6
    response = (
        "Tolong spesifikasikan tanggalnya dari hari yang diinginkan. "
        "Pada tanggal berapa kamu akan mengakhiri jangka waktu?"
    )
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('date-range-end-month', 'default response'))
    mocker.patch('kumabot.handlers.set_range_month_only', return_value=('no-date', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'kuma sampai juni'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_set_range_end_no_month(mocker):
    response = "Pada bulan apa kamu akan mengakhiri jangka waktu?"
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('date-range-end-day', 'default response'))
    mocker.patch('kumabot.handlers.set_range_day_only', return_value=('no-month', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'kuma sampai tanggal 27'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_set_range_end_no_date_month(mocker):
    response = (
        "Tolong spesifikasikan tanggal dan bulan yang diinginkan. "
        "Pada tanggal berapa dan bulan apa kamu "
        "akan mengakhiri jangka waktu?"
    )
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('date-range-end-day', 'default response'))
    mocker.patch('kumabot.handlers.set_range_day_only', return_value=('no-date-month', group))

    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'kuma sampai jumat'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_set_range_end_no_valid(mocker):
    group.range_start = '2017-06-28'
    response = (
        "Mohon maaf, tanggal yang anda masukkan tidak valid. "
        "Silahkan memasukkan kembali tanggal akhir yang diinginkan."
    )
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('date-range-end', 'default response'))
    mocker.patch('kumabot.handlers.set_end_range', return_value=('no-valid', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'kuma sampai 28 april 2017'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_set_range_end_fail_db_error(mocker):
    group.range_start = '2018-06-28'
    response = (
        "Mohon maaf, ada gangguan pada database. "
        "Silahkan masukkan kembali tanggal akhir yang diinginkan."
    )
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('date-range-end', 'default response'))
    mocker.patch('kumabot.handlers.set_end_range', return_value=('fail', group))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'kuma sampai 27 juni 2018'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_add_schedule_available_month_success(mocker):
   response = (
       "Tanggal jadwal kosongmu sudah tersimpan yaitu "
       "26-6-2018 pada jam 7:30."
   )
   mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
   mocker.patch('kumabot.handlers.identify_intent',
                return_value=('available-schedule-month', 'default response'))
   mocker.patch('kumabot.handlers.add_available_schedule_month', return_value=('success', user))
   mock_message = Mock()
   mock_message.source.type = 'group'
   mock_message.source.group_id = '123'
   mock_message.message.text = 'kuma aku bisa bulan juni'
   handle_message(mock_message)

   args, _ = mocked_reply_to.call_args
   expected_text = (TextSendMessage(response))
   assert args[1].text == expected_text.text
#
#
#def test_handle_add_schedule_available_day_month_success(mocker):
#    user.year = 2018
#    user.time = 7
#    group.range_start = '2018-10-28'
#    group.range_end = '2018-11-28'
#    response = (
#        "Tanggal jadwal kosong fellita sudah tersimpan yaitu "
#        "1-11-2018 pada jam 7."
#    )
#    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
#    mocker.patch('kumabot.handlers.identify_intent',
#                 return_value=('available-schedule-day-month', 'default response'))
#    mocker.patch('kumabot.handlers.add_available_schedule_day_month', return_value=('success', group))
#    mock_message = Mock()
#    mock_message.source.type = 'group'
#    mock_message.source.group_id = '123'
#    mock_message.message.text = 'kuma saya bisa nya tanggal 1 bulan november'
#    handle_message(mock_message)
#
#    args, _ = mocked_reply_to.call_args
#    expected_text = (TextSendMessage(response))
#    assert args[1].text == expected_text.text
#
#
#def test_handle_add_schedule_available_month_time_success(mocker):
#    user.year = 2018
#    user.day = 1
#    group.range_start = '2018-10-28'
#    group.range_end = '2018-11-28'
#    response = (
#        "Tanggal jadwal kosong fellita sudah tersimpan yaitu "
#        "1-11-2018 pada jam 7."
#    )
#    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
#    mocker.patch('kumabot.handlers.identify_intent',
#                 return_value=('available-schedule-month-time', 'default response'))
#    mocker.patch('kumabot.handlers.add_available_schedule_month_time', return_value=('success', group))
#    mock_message = Mock()
#    mock_message.source.type = 'group'
#    mock_message.source.group_id = '123'
#    mock_message.message.text = 'kuma bisa bulan november jam 7 say'
#    handle_message(mock_message)
#
#    args, _ = mocked_reply_to.call_args
#    expected_text = (TextSendMessage(response))
#    assert args[1].text == expected_text.text


def test_handle_add_available_schedule_day(mocker):
    response = (
        "Untuk tanggal {}, bulan {}, pada jam berapa kamu "
        "memiliki waktu kosong?"
    ).format(user.day, user.month)
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule-day', 'default response'))
    mocker.patch('kumabot.handlers.add_available_schedule_day', return_value=('ask-time', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'aku bisa tanggal 26 kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_add_available_schedule_day_time(mocker):
    response = (
        "Pada bulan apa kamu memiliki waktu kosong di"
        " tanggal {} jam {}:{}?").format(user.day, user.time[0], user.time[1])
    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule-day-time', 'default response'))
    mocker.patch('kumabot.handlers.add_available_schedule_day_time', return_value=('ask-months', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'aku bisa tanggal 26 jam 5 kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_add_schedule_available_success(mocker):
    response = (
        "Tanggal jadwal kosongmu sudah tersimpan yaitu "
        "{}-{}-{} pada jam {}:{}."
    ).format(user.day, user.month, 2018, user.time[0], user.time[1])

    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule', 'defaut response'))
    mocker.patch('kumabot.handlers.add_available_schedule', return_value=('success', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'bisa tanggal 26 juni kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_add_schedule_available_fail(mocker):
    response = (
        "Mohon maaf, ada gangguan pada database. "
        "Silahkan memasukkan kembali tanggal jadwal kosong anda.")

    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule', 'defaut response'))
    mocker.patch('kumabot.handlers.add_available_schedule', return_value=('fail', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'bisa tanggal 26 juni kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_add_schedule_available_no_valid(mocker):
    response = (
        "Mohon maaf, tanggal yang anda masukkan tidak valid. "
        "Silahkan memasukkan kembali tanggal jadwal kosong anda.")

    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule', 'defaut response'))
    mocker.patch('kumabot.handlers.add_available_schedule', return_value=('no-valid', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'bisa tanggal 23 juni kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_add_schedule_available_month_time(mocker):
    response = (
        "Pada tanggal berapa kamu memiliki waktu kosong di"
        " Bulan {} jam {}:{}?").format(user.month, user.time[0], user.time[1])

    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule-month-time', 'defaut response'))
    mocker.patch('kumabot.handlers.add_available_schedule_month_time', return_value=('ask-dates', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'bisa bulan juni jam 7:30 kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_add_schedule_available_month(mocker):
    response = (
        "Pada tanggal berapa kamu memiliki waktu kosong di"
        " Bulan {}?").format(user.month)

    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule-month', 'defaut response'))
    mocker.patch('kumabot.handlers.add_available_schedule_month', return_value=('ask-dates-month', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'bisa bulan juni kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_add_schedule_available_ask_date(mocker):
    user.schedule_avb = [datetime.date(2018, 6, 25), datetime.date(2018, 6, 26), datetime.date(2018, 6, 27),
                datetime.date(2018, 6, 28), datetime.date(2018, 6, 29), datetime.date(2018, 6, 30)]
    response = (
        "Dari hari yang sudah anda pilih terdapat beberapa tanggal dengan"
        " hari yang sama yaitu tanggal 25, 26, 27, 28, 29.\n"
        "Pada tanggal berapa kamu memiliki jadwal kosong?")

    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule', 'defaut response'))
    mocker.patch('kumabot.handlers.add_available_schedule', return_value=('ask-date', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'bisa bulan juni jam 7.30 hari kamis kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_add_schedule_available_day_month(mocker):
    response = (
        "Pada tanggal berapa kamu memiliki waktu kosong di"
        " Bulan {}?").format(user.month)

    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule-day-month', 'defaut response'))
    mocker.patch('kumabot.handlers.add_available_schedule_day_month', return_value=('ask-dates-month', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'bisa bulan juni hari kamis kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_add_schedule_available_day_ask_month(mocker):
    user.schedule_avb = [datetime.date(2018, 6,25), datetime.date(2018, 7,25),
        datetime.date(2018, 8,25), datetime.date(2018, 9,25),
        datetime.date(2018, 10,25), datetime.date(2018, 11,25)]
    response = (
        "Pada tanggal yang kamu pilih, terdapat tanggal di beberapa"
        "bulan yang berbeda yaitu pada Bulan Juni, Juli, Agustus, September, Oktober.\n"
        "Pada Bulan apa kamu memiliki waktu kosong?")

    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule-day', 'defaut response'))
    mocker.patch('kumabot.handlers.add_available_schedule_day', return_value=('ask-month', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'bisa tanggal 25 kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_handle_add_schedule_available_time(mocker):
    response = (
        "Pada tanggal berapa kamu memiliki waktu kosong di"
        " jam {}:{}?").format(user.time[0], user.time[1])

    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule-time', 'defaut response'))
    mocker.patch('kumabot.handlers.add_available_schedule_time', return_value=('ask-date-time', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'bisa jam 7.30 kum'
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_next_handler_available_day(mocker):
    message.register_next_handler(user.id, next_handler_available_day)
    response = (
        "Anda sudah memasukkan jadwal yang sama sebelumnya.")

    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule-day', 'defaut response'))
    mocker.patch('kumabot.handlers.add_available_schedule_day', return_value=('already-set', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'bisa tanggal 25 kum'
    group.users = {mock_message.source.user_id : user}
    mocker.patch.dict('kumabot.handlers.groups', return_value={mock_message.source.group_id : group})
    message.register_next_handler(mock_message.source.user_id, next_handler_available_day)
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_next_handler_available_month(mocker):
    message.register_next_handler(user.id, next_handler_available_day)
    response = (
        "Anda sudah memasukkan jadwal yang sama sebelumnya.")

    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule-month', 'defaut response'))
    mocker.patch('kumabot.handlers.add_available_schedule_month', return_value=('already-set', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'bisa bulan juni kum'
    group.users = {mock_message.source.user_id : user}
    mocker.patch.dict('kumabot.handlers.groups', return_value={mock_message.source.group_id : group})
    message.register_next_handler(mock_message.source.user_id, next_handler_available_month)
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text


def test_next_handler_available_time(mocker):
    message.register_next_handler(user.id, next_handler_available_day)
    response = (
        "Anda sudah memasukkan jadwal yang sama sebelumnya.")

    mocked_reply_to = mocker.patch('kumabot.handlers.bot.reply_message')
    mocker.patch('kumabot.handlers.identify_intent',
                 return_value=('available-schedule-time', 'defaut response'))
    mocker.patch('kumabot.handlers.add_available_schedule_time', return_value=('already-set', user))
    mock_message = Mock()
    mock_message.source.type = 'group'
    mock_message.source.group_id = '123'
    mock_message.message.text = 'bisa jam 7.30 kum'
    group.users = {mock_message.source.user_id : user}
    mocker.patch.dict('kumabot.handlers.groups', return_value={mock_message.source.group_id : group})
    message.register_next_handler(mock_message.source.user_id, next_handler_available_time)
    handle_message(mock_message)

    args, _ = mocked_reply_to.call_args
    expected_text = (TextSendMessage(response))
    assert args[1].text == expected_text.text
